const pool = require(`../lib/mysql`);

module.exports = class User {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }

    static getUsers() {
        return pool.query(`select * from employees`);
    }

    static createUser(name) {
        return pool.query(`insert into employees (name) values (?)`, [name]);
    }

    static getUser(id) {
        return pool.query(`select * from employees where id = ?`, [id]);
    }

    static updateUser(id, name) {
        return pool.query(`update employees set name = ? where id = ?`, [name, id]);
    }

    static deleteUser(id) {
        return pool.query(`delete from employees where id = ?`, [id]);
    }
}