const pool = require(`./../lib/mysql`);

const getUsers = async () => {
    try {
        return pool.query(`select * from employees`);
    } catch (error) {
        throw error;
    }
}

const createUser = async ({ name }) => {
    try {
        return pool.query(`insert into employees (name) values (?)`, [name]);
    } catch (error) {
        throw error;
    }
}

const getUser = async (id) => {
    try {
        return pool.query(`select * from employees where id = ?`, [id]);
    } catch (error) {
        throw error;
    }
}

const updateUser = async (id, name) => {
    try {
        return pool.query(`update employees set name = ? where id = ?`, [name, id]);
    } catch (error) {
        throw error;
    }
}

const deleteUser = async (id) => {
    try {
        return pool.query(`delete from employees where id = ?`, [id]);
    } catch (error) {
        throw error;
    }
}

module.exports = {
    getUsers,
    createUser,
    getUser,
    updateUser,
    deleteUser
}