/**
 * @swagger
 * /users/:
 *  get:
 *      tags:
 *        - Users
 *      summary: Resturns Users
 *      description: List of users
 *      produces:
 *          application/json
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Fail.
 */

/**
 * @swagger
 * /users/{id}:
 *  get:
 *      tags:
 *        - Users
 *      summary: Resturns User
 *      description: User
 *      produces:
 *          application/json
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Fail.
 */

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      required:
 *          - name
 *      properties:
 *          name:
 *              type: string
 *              required: true
 *              example: 'Alexa G'
 */

/**
 * @swagger
 * /users/:
 *  post:
 *      tags:
 *          - Users
 *      summary: Create Users
 *      description: Create new user
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: body
 *            name: request
 *            required: true
 *            schema:
 *              $ref: '#/definitions/User'
 *      responses:
 *          201:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Fail.
 */

/**
 * @swagger
 * /users/{id}:
 *  put:
 *      tags:
 *          - Users
 *      summary: Update Users
 *      description: Update user
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *          - in: body
 *            name: request
 *            required: true
 *            schema:
 *              $ref: '#/definitions/User'
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Fail.
 */

/**
 * @swagger
 * /users/{id}:
 *  delete:
 *      tags:
 *          - Users
 *      summary: Delete User
 *      description: Delete user
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Fail.
 */