var express = require('express');
var router = express.Router();
const User = require(`./../services/user`);

router.get('/', async (req, res, next) => {
  try {
    const [users] = await User.getUsers();
    res.status(200).json({ message: `success`, users });
  } catch (error) {
    res.status(400).json(error);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const user = await User.createUser(req.body.name);
    res.status(201).json({ message: `success` });
  } catch (error) {
    res.status(400).json(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const [user] = await User.getUser(req.params.id);
    res.status(200).json({ message: `success`, user });
  } catch (error) {
    res.status(400).json(error);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    const user = await User.updateUser(req.params.id, req.body.name);
    res.status(200).json({ message: `success` });
  } catch (error) {
    res.status(400).json(error);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const user = await User.deleteUser(req.params.id);
    res.status(200).json({ message: `success` });
  } catch (error) {
    res.status(400).json(error);
  }
});

module.exports = router;